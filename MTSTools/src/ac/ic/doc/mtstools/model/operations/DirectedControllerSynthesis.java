package ac.ic.doc.mtstools.model.operations;

import java.util.AbstractSet;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import ac.ic.doc.commons.relations.BinaryRelation;
import ac.ic.doc.commons.relations.BinaryRelationImpl;
import ac.ic.doc.commons.relations.Pair;
import ac.ic.doc.mtstools.model.LTS;
import ac.ic.doc.mtstools.model.impl.LTSImpl;



/** This class contains the logic to synthesize a controller for
 *  a deterministic environment using an informed search procedure. */
public class DirectedControllerSynthesis<State, Action> {

	/** Constant used to represent an infinite distance to the goal. */
	public static final int INF = Integer.MAX_VALUE;
	
	/** Constant used to represent an undefined distance to the goal. */
	public static final int UNDEF = -1;
	
	/** List of LTS names (used for debugging). */
	private List<String> names;
	
	/** List of LTS that compose the environment. */
	private List<LTS<State,Action>> ltss;
	
	/** Actions the controller has to reach (at least one). */
	private Set<Action> reach;
	
	/** Actions the controller has to avoid (all of them). */
	private Set<Action> avoid;
	
	/** Set of controllable actions. */
	private Set<Action> controllable;
	
	/** Environment alphabet (implements perfect hashing for actions). */
	private Alphabet alphabet;
	
	/** Set of transitions enabled by default by each LTS. */
	private TransitionSet base;
	
	/** Auxiliary transitions allowed by a given compostate. */
	private TransitionSet allowed;
	
	/** Last states used to updated the allowed transitions. */
	private List<State> facilitators;
	
	/** Heuristic evaluator used to rank the transitions from a state. */
	private HeuristicEvaluator heuristicEvaluator;
	
	/** Queue of open states, the most promising state should be expanded first. */
	private Queue<Compostate> open;
	
	/** Queue of states evaluated with the heuristic yet to be expanded.*/
	private Queue<Compostate> evaluated;
	
	/** Cache of states mapped from their basic components. */
	private Map<List<State>,Compostate> compostates;
	
	/** List of recently expanded states yet to be opened. */
	private Deque<Compostate> expanded;
	
	/** List of reachable states after a transition (used during expansion). */
	private Deque<Set<State>> transitions;
	
	/** List of ancestors of an state (used during propagation). */
	private Deque<BinaryRelation<HAction,Compostate>> ancestors;
	
	/** Set of states to be closed after goal propagation. */
	private List<Compostate> closed;
	
	/** List of descendants of an state (used to close unnecessary descendants). */
	private Deque<Compostate> descendants;
	
	/** Initial state. */
	private Compostate initial;
	
	/** Statistic information about the procedure. */
	private Statistics statistics = new Statistics();
	
	
	/** This method starts the directed synthesis of a controller.
	 *  @param names, the names of the LTSs (for debugging).
	 *  @param ltss, a list of LTSs that compose the environment. 
	 *  @param controllalbe, the set of controllable actions.
	 *  @param reach, the action set to-be-reached by the controller.
	 *  @param avoid, the action set to-be-avoided by the controller.
	 *  @return the controller for the environment in the form of an LTS that
	 *      when composed with the environment reaches the goal, only by
	 *      enabling or disabling controllable actions and monitoring
	 *      uncontrollable actions. Or null if no such controller exists. */
	public LTS<Long,Action> synthesize(
			List<String> names,
			List<LTS<State,Action>> ltss,
			Set<Action> controllable,
			Set<Action> reach,
			Set<Action> avoid)
	{
		this.names = names;
		this.ltss = ltss;
		this.reach = reach;
		this.avoid = avoid;
		this.controllable = controllable;
		
		statistics.clear();
		statistics.start();
		
		open = new PriorityQueue<>();
		evaluated = new LinkedList<>();
		
		compostates = new HashMap<>();
		expanded = new ArrayDeque<>();
		transitions = new ArrayDeque<>(ltss.size());
		ancestors = new ArrayDeque<>();
		closed = new ArrayList<>();
		descendants = new ArrayDeque<>();

		alphabet = new Alphabet();
		base = new TransitionSet(alphabet);
		allowed = base.clone();
		heuristicEvaluator = new HeuristicEvaluator();

		initial = buildInitialState();
		initial.open();
		
		LTS<Long,Action> result = null;

		while (!isGoal(initial) && !isError(initial)) {
			doEval();
			doOpen();
		}
		
		statistics.end();
		result = buildController();
			
		return result;
	}
	
	
	/** Returns the statistic information about the procedure. */
	public Statistics getStatistics() {
		return statistics;
	}
	
	
	/** Creates (or retrieves from cache) a state in the composition given
	 *  a list with its base components.
	 *  @param states, a list of states with one state per LTS in the
	 *         environment, the position of each state in the list reflects
	 *         to which LTS that state belongs. */
	private Compostate buildCompostate(List<State> states) {
		Compostate result = compostates.get(states);
		if (result == null) {
			result = new Compostate(states);
			compostates.put(states, result);
			statistics.incExpanded();
		}
		return result;
	}
	
	
	/** Creates the controller's initial state. */
	private Compostate buildInitialState() {
		List<State> states = new ArrayList<>(ltss.size());
		for (LTS<State,Action> lts : ltss)
			states.add(lts.getInitialState());
		Compostate result = buildCompostate(states); // for non-deterministic LTS I need the tau-closure as a set of initial states
		return result;
	}
	
	
	/** Evaluates through the heuristic the next state to open (if necessary). */
	private void doEval() {
		Compostate compostate = open.remove();
		if (compostate.isLive()) {
			if (!compostate.isEvaluated()) { // redundant
				heuristicEvaluator.eval(compostate);
			} else {
				statistics.incRevisited();
			}
		}
		evaluated.add(compostate);
	}
	
	
	/** Expands the next recommendation of an open state and opens its child.
	 *  If no further recommendations are available the state is marked as an
	 *  error, and the error is propagated to its ancestors.
	 *  If the state is controllable and has further recommendations the parent
	 *  state is left open and competes with its children in the open queue.
	 *  This avoids a DFS type of search. On the other hand, uncontrollable
	 *  states are never left opened since all its children need to be explored
	 *  eventually, thus a DFS to a failure is used instead. */
	private void doOpen() {
		Compostate compostate = evaluated.remove();
		if (compostate.getStates() == null)
			return;
		compostate.inOpen = false;
		if (!compostate.hasValidRecommendation()) {
			if (isError(compostate) || compostate.getChildren().isEmpty())
				propagateError(compostate);
			else if (isGoal(compostate))
				propagateGoal(compostate);
			// what happens if it is a loop?
		} else {
			Recommendation recommendation = compostate.nextRecommendation();
			statistics.setEstimate(recommendation.getEstimate());
			expand(compostate, recommendation);
			openChildren(compostate, recommendation);
			if (compostate.isControlled() && compostate.hasValidRecommendation() && !isGoal(compostate))
				compostate.open();
		}
	}
	
	
	/** Expands a state following a given recommendation from a parent compostate.
	 *  Internally this populates the transitions and expanded lists. */
	private void expand(Compostate compostate, Recommendation recommendation) { // I am working in a compostate basis, but I could work in a bisimilar partition and produce a minimal controller. This would reduce repetition, but increase the cost of maintaining the partitions.
		HAction action = recommendation.getAction();
		List<State> parentStates = compostate.getStates();
		int size = parentStates.size();
		for (int i = 0; i < size; ++i) {
			Set<State> image = ltss.get(i).getTransitions(parentStates.get(i)).getImage(action.getAction());
			if (image == null || image.isEmpty()) {
				if (ltss.get(i).getActions().contains(action.getAction())) { // tau does not belong to the lts alphabet, yet it may have a valid image, but I do not want taus at this stage
					transitions.clear();
					System.err.println("Invalid action: " + action);
					return; // do not expand an state through an invalid action
				}
				image = Collections.singleton(parentStates.get(i));
			}
			transitions.add(image);
		}
		List<State> childStates = new ArrayList<>();
		for (Set<State> singleton : transitions) // cartesian for non-deterministic states
			childStates.add(singleton.iterator().next());
		transitions.clear();
		Compostate child = buildCompostate(childStates);
		compostate.addChild(action, child);
		child.addParent(action, compostate);
		if (recommendation.getEstimate() == 1)
			compostate.setDistance(1);
		if (!isGoal(child))
			child.setEstimate(recommendation.getEstimate()-1);
		expanded.add(child);
	}
	
	
//	/** Computes the cartesian product between multiple sets of states.
//	 *  Internally this is used to consume transitions produced by the expand
//	 *  method, while producing the list of states of the new states. */
//	private Deque<List<State>> cartesian(Deque<Set<State>> statesByLTS) { // non-deterministic states should be modeled with a set of lists, thus cartesian should be used while expanding a new state
//		Deque<List<State>> result = new ArrayDeque<>();
//		Deque<List<State>> buffer = new ArrayDeque<>();
//		Deque<List<State>> swap;
//		result.add(new ArrayList<State>());
//		while (!statesByLTS.isEmpty()) {
//			Set<State> states = statesByLTS.pop();
//			while (!result.isEmpty()) {
//				List<State> list = result.pop();
//				for (State state : states) {
//					List<State> newList = new ArrayList<>(list);
//					newList.add(state);
//					buffer.add(newList);
//				}
//			}
//			swap = result;
//			result = buffer;
//			buffer = swap;
//		}
//		return result;
//	}
	
	
	/** Open the children of a given state following a recommendation.
	 *  Internally this consumes states from the expanded list produced by
	 *  the expand method and effectively opens them. This method also checks
	 *  if the children have already been flagged as errors or goals and
	 *  propagates the information accordingly. */
	private void openChildren(Compostate compostate, Recommendation recommendation) {
		while (!expanded.isEmpty()) {
			Compostate child = expanded.pop();
			//System.out.print(recommendation + " --> ");
			if (isGoal(child))
				propagateGoal(child, recommendation.getAction(), compostate);
			else if (isError(child)) 
				propagateError(child, recommendation.getAction(), compostate);
			else if (child.isLive() && isAncestor(child, compostate))
				propagateError(child, recommendation.getAction(), compostate);
			else
				child.open();
		}
		// Note: if one of the non-deterministic child is an error cut everything
	}
	
	
	/** Determines whether the precursor state is an ancestor of the successor
	 *  state by iteratively going backwards through the parent relation. */
	private boolean isAncestor(Compostate precursor, Compostate successor) {
		boolean result = false;
		addAncestors(successor);
		while (!ancestors.isEmpty()) {
			for (Pair<HAction,Compostate> predecesor : ancestors.pop()) {
				Compostate predState = predecesor.getSecond();
				if (result = predState.equals(precursor)) {
					ancestors.clear();
					break;
				}
				addAncestors(predState);
			}
		}
		return result;
	}
	
	
	/** Given a state known to be an error this method propagates this
	 *  information back to its ancestors.
	 *  @see propagateError() */
	private void propagateError(Compostate compostate) {
		addAncestors(compostate);
		compostate.releaseParents();
		propagateError();
	}
	

	/** Given an ancestor relation (action,parent) from a child known to be
	 *  a goal this method propagates this information back to its ancestors.
	 *  @see propagateError() */
	private void propagateError(Compostate child, HAction action, Compostate parent) {
		BinaryRelation<HAction, Compostate> heir = new BinaryRelationImpl<>();
		heir.addPair(action, parent);
		ancestors.addLast(heir);
		child.removeParent(action, parent);
		propagateError();
	}
	
	
	/** Given an ancestor relation in the ancestors internal list from a
	 *  state known to be an error this method propagates this information back.
	 *  Uncontrollable ancestors are marked as errors since we need all of
	 *  their children to be goals. Then the other descendants are also closed.
	 *  Controllable ancestors with remaining options stop the propagation,
	 *  since we only need one of its children to reach the goal. However,
	 *  exhausted controllable state are marked as errors and continue the
	 *  propagation. */
	private void propagateError() {
		while (!ancestors.isEmpty()) {
			for (Pair<HAction,Compostate> predecesor : ancestors.pop()) {
				Compostate predState = predecesor.getSecond();
				if (predState.isControlled()) { // controllable
					closeDescendants(predecesor.getFirst(), false, predState); // close non-deterministic children
					if (!predState.hasValidRecommendation() && predState.getChildren().isEmpty()) {
						addAncestors(predState);
						setError(predState);
					}
				} else { // uncontrollable
					closeDescendants(null, false, predState); // close all children
					addAncestors(predState);
					setError(predState);
				}
			}
		}
		heuristicEvaluator.clearSuggestions(); // force recomputation of the abstraction
	}
	
	
	/** Given a state known to be a goal this method propagates this
	 *  information back to its ancestors. 
	 *  @see propagateGoal() */
	private void propagateGoal(Compostate compostate) {
		addAncestors(compostate);
		compostate.releaseParents();
		propagateGoal();
	}
	
	
	/** Given an ancestor relation (action,parent) from a child known to be
	 *  a goal this method propagates this information back to its ancestors.
	 *  @see propagateGoal() */
	private void propagateGoal(Compostate child, HAction action, Compostate parent) {
		BinaryRelation<HAction, Compostate> heir = new BinaryRelationImpl<>();
		heir.addPair(action, parent);
		ancestors.addLast(heir);
		propagateGoal();
	}
	
	
	/** Given an ancestor relation in the ancestors internal list from a state
	 *  known to be a goal this method propagates this information back.
	 *  Controllable ancestors are marked as goals and continue the propagation,
	 *  since we need only one of its children to reach the goal.
	 *  Uncontrollable states with remaining options stop the propagation,
	 *  since we need all of their children to reach the goal. Thus, we
	 *  reopen them to allow the exploration to continue. Exhausted
	 *  uncontrollable states can be safely marked as goals and allow to
	 *  continue with the propagation*/
	private void propagateGoal() {
		while (!ancestors.isEmpty()) {
			for (Pair<HAction,Compostate> predecesor : ancestors.pop()) {
				HAction predAction = predecesor.getFirst();
				Compostate predState = predecesor.getSecond();
				if (predState.isControlled()) { // controllable
					// if (isGoalParent(predState, predAction)) { // all non-deterministic children lead to a goal
						addAncestors(predState);
						setGoal(predState, predAction);
						closed.add(predState);
					// }
				} else { // uncontrollable
					// if (isGoalParent(predState, predAction)) { // all non-deterministic children lead to a goal
					if (predState.hasValidRecommendation()) {
							predState.open();
						} else {
							addAncestors(predState);
							setGoal(predState, predAction);
						}
					// }
				}
			}
		}
		for (Compostate state : closed) // after a sequence of goal states has been marked close the other children
			closeDescendants(null, true, state);
		closed.clear();
	}
	
	
	/** Adds the parents of a given state to the ancestor list (used during
	 *  goal and error propagation). */
	private void addAncestors(Compostate compostate) {
		BinaryRelation<HAction,Compostate> parents = compostate.getParents();
		if (parents != null)
			ancestors.add(parents);
	}
	
	
//	/** Checks if a given state is the parent of a goal state following a given action. */
//	private boolean isGoalParent(Compostate compostate, Action action) {
//		boolean result = true;
//		Iterator<Compostate> it = compostate.getChildren().getImage(action).iterator();
//		while (result && it.hasNext())
//			result &= isGoal(it.next());
//		return result;		
//	}
	
	
	/** Closes, that is removes from the open list, all the states that descend
	 *  from a given ancestor. If this action is triggered by a goal propagation
	 *  only non-goal-reaching descendants are closed. On the other hand, if
	 *  triggered by an error propagation, only the error-reaching children are
	 *  closed. 
	 *  @param action, the action that lead to closing the descendants.
	 *         If the action leads to a goal all the other children are closed.
	 *         If the action leads to an error only those children are closed.
	 *         If the action is null all children are closed.
	 *  @param goal, boolean whether the action leads to a goal or an error.
	 *  @param compostate, the ancestor from which descendants are to be closed. */
	private void closeDescendants(HAction action, boolean goal, Compostate compostate) {
		for (Pair<HAction,Compostate> transition : compostate.getChildren()) {
			HAction childAction = transition.getFirst();
			Compostate childState = transition.getSecond();
			if (compostate.isObjective(childAction)) // avoid closing children that lead to objectives
				continue;
			if (action == null || (childAction.equals(action) ? !goal : goal)) {
				compostate.removeChild(childAction);
				childState.removeParent(childAction, compostate);
				if (!childState.hasLiveParent())
					descendants.add(childState);
			}
		}
		while (!descendants.isEmpty()) {
			Compostate successor = descendants.pop();
			if (successor.isLive()) {
				successor.close();
				for (Pair<HAction, Compostate> transition : successor.getChildren()) {
					Compostate succState = transition.getSecond();
					if (!succState.hasLiveParent())
						descendants.add(succState);
				}
			}
		}
	}
	
	
	/** Returns whether a given state is an error or not. */
	private boolean isError(Compostate compostate) {
		return compostate.getEstimate() == INF;
	}
	
	
	/** Marks a given state as an error. */
	private void setError(Compostate compostate) {
		setFinal(compostate, false);
	}
	
	
	/** Returns whether a given state is a goal or not. */
	private boolean isGoal(Compostate compostate) {
		return compostate.getEstimate() == 0;
	}
	
	
	/** Marks a given state as a goal. */
	private void setGoal(Compostate compostate, HAction action) {
		compostate.updateDistance(action);
		compostate.addObjective(action);
		setFinal(compostate, true);
	}
	
	
	/** Marks a given state as final, closing it and releasing some resources. */
	private void setFinal(Compostate compostate, boolean goal) {
		compostate.close();
		compostate.setEstimate(goal ? 0 : INF);
		compostate.clearRecommendations();
		compostate.releaseParents();
	}
	
	
	/** After the synthesis procedure this method builds a controller in the 
	 *  form of an LTS by starting in the initial state and following all the
	 *  non-closed descendants. If there is no controller for the given
	 *  environment this method returns null. */
	private LTS<Long, Action> buildController() {
		if (isError(initial))
			return null;
		long i = 0;
		LTSImpl<Long, Action> result = new LTSImpl<Long, Action>(i);
		Map<Compostate, Long> ids = new HashMap<>();
		ids.put(initial, i++);
		descendants.add(initial);
		Set<Pair<HAction,Compostate>> transitions = new HashSet<>();
		while (!descendants.isEmpty()) {
			Compostate successor = descendants.poll();
			if (successor.isControlled() && successor.distance != 1) {
				BinaryRelationImpl.RelPair<HAction,Compostate> best =
					new BinaryRelationImpl.RelPair<>();
				int distance = INF;
				for (Pair<HAction,Compostate> transition : successor.getChildren()) {
					int update = transition.getSecond().getDistance();
					if (distance > update) {
						distance = update;
						best.copy(transition);
					}
				}
				if (best.getFirst() != null)
					transitions.add(best); // only add the best controllable action
			} else {
				for (Pair<HAction, Compostate> transition : successor.getChildren()) // add all uncontrollable actions
					transitions.add(new Pair<>(transition.getFirst(), transition.getSecond()));
			}
			for (Pair<HAction,Compostate> transition : transitions) {
				Action action = transition.getFirst().getAction();
				Compostate child = transition.getSecond();
				if (!ids.containsKey(child)) {
					ids.put(child, i++);
					descendants.add(child);
				}
				result.addState(ids.get(child));
				result.addAction(action); // I should add all the alphabet not only the reachable part...
				result.addTransition(ids.get(successor), action, ids.get(child));
			}
			transitions.clear();
		}
		statistics.setUsed(result.getStates().size());
		return result;
	}
	
	
	
	/** This class holds the logic to make an heuristic evaluation of a given
	 *  state. This works by building an abstraction and computing an estimate
	 *  of the distance to a goal for each available action. The estimates can
	 *  then be used to rank the transitions to explore. */
	private class HeuristicEvaluator {
		
		/** Set of transitions enabled by each LTS. */
		private TransitionSet enabled;
		
		/** States considered during an execution of the evaluator. */
		private Set<HState> states;
		
		/** States from the "generation" in which back-propagation is working. */
		private Set<HState> update;
		
		/** States that have not been previously considered and that are now available. */
		private Set<HState> fresh;
		
		/** States known to be deadlocks. */
		private Set<HState> deadlocks;
		
		/** States known to be errors. */
		private Set<HState> errors;
		
		/** Set of actions that are enabled by all the LTSs. */
		private Set<HAction> ready;
		
		/** States with unpromoted children indexed by the number of such children. */
		private Map<Integer, Set<HState>> unpromoted;
		
		/** Set with the numbers of unpromoted children. */
		private BitSet missing;
		
		/** Index of the sets with minimum missing children. */
		private int index;
		
		/** Number of generations in the abstraction (incremental while building). */
		private int generations;
		
		/** Maps for each state known to be a goal the actions and parents that led to them. */
		private Map<HState, BinaryRelation<HAction,HState>> goals;
		
		/** Pending transitions for a given action. */
		private Map<HAction, Pair<Set<HState>,Set<HState>>> pending;
		
		/** Previously processed transitions that may enable cross-LTS steps from fresh states. */
		private Map<HAction, Set<HState>> enablers;
		
		/** Cache of heuristic states used to reduce dynamic allocations. */
		private Map<Integer, HashMap<State, Integer>> cache;
		
		/** Cache of heuristic states ordered by creation order (used for perfect hashing). */
		private List<HState> stash;
		
		/** Keeps the computed estimate for each transition. */
		private HTransitionMap allSuggestions;
		
		/** Keeps the computed estimates only for generation 0 states. */
		private HTransitionMap gen0Suggestions;
		
		/** Keeps previously action ranking advice for each state. */
		private Map<HState, Map<HAction, Integer>> advice;
		
		
		/** Constructor for an heuristic evaluator. */
		public HeuristicEvaluator() {
			index = 0;
			states = new HSet();
			deadlocks = new HSet();
			errors = new HSet();
			update = new HashSet<>();
			fresh = new HashSet<>();
			stash = new ArrayList<>();
			ready = new HashSet<>();
			goals = new HashMap<>();
			pending = new HashMap<>();
			enablers = new HashMap<>();
			cache = new HashMap<>();
			unpromoted = new HashMap<>();
			missing = new BitSet();
			enabled = base.clone();
			allSuggestions = new HTransitionMap();
			gen0Suggestions = new HTransitionMap();
			advice = new HashMap<>();
		}
		
		
		/** Evaluates the heuristic for a given state and sets the ranking of recommendations. */
		private void eval(Compostate compostate) {
			populateFresh(compostate);
			if (!compostate.isEvaluated() && !checkVapidRanking(compostate)) {
				init(compostate);
				buildMonotonic(compostate);
				if (!goals.isEmpty()) {
					backPropagateEstimates(compostate);
				} else { // goal unreachable (iff the heuristic proves to be admissible)
					compostate.setEstimate(INF);
				}
				statistics.incEvaluated(); // do not consider vapid states as evaluated
			}
		}
		
		

		/** Returns whether with a previously computed monotonic abstraction
		 *  we can give an estimate for the currently available transitions.
		 *  If there is at least one available action that we cannot estimate
		 *  or if an error was propagated recently returns false. */
		private boolean checkVapidRanking(Compostate compostate) { // I should think of other conditions to force the recomputation of the heuristic
			return !allSuggestions.isEmpty() && extractRecommendations(compostate, allSuggestions); 
		}
		
		
		/** Initialize the heuristic evaluator state. */
		private void init(Compostate compostate) {
			clear();
			enabled.copy(base); 
		}
		
		
		/** Clears cached suggestions obtained by the last execution of the heuristic. */
		private void clearSuggestions() {
			allSuggestions.clear();
			advice.clear();
		}
		
		
		/** Builds (or retrieves from cache) an heuristic state. */
		private HState buildHState(int lts, State state) {
			HashMap<State, Integer> table = cache.get(lts); 
			if (table == null) {
				table = new HashMap<State, Integer>();
				cache.put(lts, table);
			}
			Integer index = table.get(state);
			if (index == null) {
				HState hstate = new HState(lts, state);
				index = hstate.hashCode();
				table.put(state, index);
			}
			return stash.get(index);
		}
		
		
		/** Initializes the fresh states with the states of a given compostate. */
		private void populateFresh(Compostate compostate) {
			fresh.clear();
			for (int i = 0; i < compostate.getStates().size(); ++i)
				fresh.add(buildHState(i, compostate.getStates().get(i)));
		}
		
		
		/** Builds the monotonic abstraction from a given state.
		 *  In this abstraction once an action is enabled is never disabled,
		 *  thus it exhausts all possible actions fast and allows to compute
		 *  an estimate distance to the goal action. */
		private void buildMonotonic(Compostate compostate) {
			for (HState state : fresh)
				state.setGeneration(0);
			states.addAll(fresh);
			deadlocks.addAll(fresh);
			for (generations = 1; updatePending(); ++generations) {
				for (HAction action : ready) {
					Pair<Set<HState>,Set<HState>> associations = pending.get(action);
					Set<HState> enabler = enablers.get(action);
					if (enabler == null)
						enablers.put(action, enabler = new HashSet<>());
					enabler.addAll(associations.getFirst());
					for (HState parent : enabler) {
						for (HState child : associations.getSecond())
							if (parent.lts != child.lts ||
									parent.getTransitions().getImage(action.getAction()).contains(child.state))
								updateFresh(parent, action, child, generations);
					}
					pending.remove(action);
				}
			}
			//System.out.println("States: " + states.size() + " - Generations: " + generations);
		}
		
		
		/** Updates pending transitions considering the last states added to
		 *  the abstraction. Internally consumes fresh states and produces
		 *  pending transitions. Returns true if pending transitions were
		 *  produced. */
		private boolean updatePending() {
			ready.clear();
			for (HState state : fresh) {
				for (Pair<Action,State> transition : state.getTransitions()) {
					HAction action = alphabet.getHAction(transition.getFirst());
					HState target = buildHState(state.lts, transition.getSecond());
					addAssociation(state, action, target);
					if (enabled.contains(action) || enabled.add(state.lts, action))
						ready.add(action);
				}
			}
			fresh.clear();
			return !ready.isEmpty();
		}
		
		
		/** If novel adds a state to the fresh queue. If this is the first child
		 *  of the given parent, the parent is removed from the error set and
		 *  assigned a generation in this step. */
		private void updateFresh(HState parent, HAction action, HState child, int generation) {
			deadlocks.remove(parent);
			if (action.isGoal()) {
				addGoal(child, action, parent, generation);
			} else {
				boolean novel = states.add(child); // states does not necessarily contains goals
				child.addParent(action, parent);
				if (action.isError()) {
					errors.add(child); // I could end with the same state in goals and errors...
				} else if (novel) {
					deadlocks.add(child);
					fresh.add(child); // I could detect other unavoidable error states and skip the add...
					child.setGeneration(generation);
				}
			}
		}
		
		
		/** Returns the pending associations (non-performed transitions) for a given action. */
		private Pair<Set<HState>,Set<HState>> getAssociations(HAction action) {
			Pair<Set<HState>,Set<HState>> associations = pending.get(action);
			if (associations == null)
				pending.put(action, associations = new Pair<Set<HState>,Set<HState>>(
					new HashSet<HState>(), new HashSet<HState>()));
			return associations;
		}
		
		
		/** Adds a pending association for two states. */
		private void addAssociation(HState source, HAction action, HState target) {
			Pair<Set<HState>,Set<HState>> associations = getAssociations(action);
			associations.getFirst().add(source);
			associations.getSecond().add(target);
		}

		
		/** Propagates estimated distances from the states marked as goals/errors
		 *  during the abstraction procedure back to the initial states.
		 *  For controllable states we take the minimum of its children.
		 *  For uncontrollable and mixed states we optimistically take the minimum
		 *  to ensure admissibility. */
		private void backPropagateEstimates(Compostate compostate) {
			populateErrors(); // set the estimate of errors first just in case the same state is an error and a goal (optimistic)
			populateGoals(compostate);
			while (!update.isEmpty()) {
				for (HState current : update) {
					if (index == 0 || current.unpropagated.size() == index) {
						for (Pair<HAction,HState> transition : current.getParents()) {
							HAction action = transition.getFirst();
							HState parent = transition.getSecond();
							int estimate = computeEstimate(parent, current.estimate, current.generation);
							int changes = parent.updateDirection(action, current, estimate);
							if (changes != 0) // if the update may propagate changes
								deferPropagation(parent);
							updateSugestions(compostate, parent, action, current, estimate);
						}
					}
				}
				promotePropagation();
			}
			populateFresh(compostate);
			extractRecommendations(compostate, gen0Suggestions); // after reaching the fix-point set the recommendations
		}
		
		
		/** Populates the update list with goal states found during the
		 *  construction of the abstraction. */
		private void populateGoals(Compostate compostate) {
			for (Entry<HState,BinaryRelation<HAction,HState>> entry : goals.entrySet()) {
				HState child = entry.getKey();
				for (Pair<HAction,HState> transition : entry.getValue()) {
					HAction action = transition.getFirst();
					HState parent = transition.getSecond();
					update.add(parent);
					updateSugestions(compostate, parent, action, child, parent.estimate);
				}
			}
		}
		
		
		/** Populates the update list with error states found during the
		 *  construction of the abstraction. */
		private void populateErrors() {
			for (HState state : deadlocks) {
				state.estimate = generations+1; // set biggest non-infinite estimate
				update.add(state);
			}
			for (HState	state : errors) {
				state.estimate = INF;
				update.add(state);
			}
		}
		
		
		/** Sets a parent state for further propagation but delays it until all
		 *  its children have been propagated or there are no other state to
		 *  propagate (which implies there is a loop in the abstraction). */
		private void deferPropagation(HState state) {
			if (state.unpropagated.isEmpty()) { // children propagation complete
				fresh.add(state);
			} else { // delay states with pending children propagations
				int postponed = state.unpropagated.size();
				missing.set(postponed);
				Set<HState> set = unpromoted.get(postponed);
				if (set == null)
					unpromoted.put(postponed, set = new HashSet<HState>());
				set.add(state);
			} // Should I send to INF a loop? No, loops in the abstraction may not be real...
		}
		
		
		/** Sets the update list with the next states to propagate from. */
		private void promotePropagation() {
			index = 0;
			Set<HState> swap;
			while (fresh.isEmpty() && !missing.isEmpty()) {
				index = missing.nextSetBit(index);
				Set<HState> set = unpromoted.get(index);
				swap = fresh;
				fresh = set;
				set = swap;
				unpromoted.put(index, set);
				missing.clear(index);
			}
			update.clear();
			swap = fresh;
			fresh = update;
			update = swap;
		}
		
		
		/** Extracts from the advice map guidance information. Also setting the
		 *  age of the guide for retrieved advice. Returns the set of states
		 *  for which advice was not retrieved. */
		private Set<HState> extractAdvice(
				Map<HState, Map<HAction, Integer>> guidance,
				Map<HAction, Integer> ages)
		{
			Set<HState> tips = fresh;
			if (!advice.isEmpty()) {
				tips = new HashSet<>();
				Map<HAction, Integer> guide;
				for (HState state : fresh) {
					guide = advice.get(state);
					if (guide == null) {
						tips.add(state);
					} else {
						guidance.put(state, guide);
						for (HAction action : guide.keySet())
							ages.put(action, state.generation);
					}
				}
			}
			return tips;
		}
		
		
		/** Iterates relevant transitions and computes guidance information.
		 *  Also setting the age of the guide for computed advice. */
		private void extractGuidance(
				Iterator<HTransition> tit,
				Map<HState, Map<HAction, Integer>> guidance,
				Map<HAction, Integer> ages)
		{
			while (tit.hasNext()) {
				HTransition transition = tit.next();
				Map<HAction, Integer> guide = guidance.get(transition.parent);
				Integer estimate = transition.estimate;
				Integer current = guide == null ? null : guide.get(transition.action);
				Integer age = ages.get(transition.action);
				if (current == null || estimate == INF || (current != INF && current > estimate) ||
						(current == estimate && age > transition.parent.generation)) {
					if (guide == null)
						guidance.put(transition.parent, guide = new HashMap<>());
					guide.put(transition.action, estimate);
					ages.put(transition.action, transition.parent.generation); /// store the generation of each guiding action (for equal estimate min generation)
				}
			}
		}
		
		
		/** Converts guidance information into an action ranking proposal. Only
		 *  considering actions actually available from a given compostate. */
		private Map<HAction, Integer> extractProposals(
				Compostate compostate,
				Map<HState, Map<HAction, Integer>> guidance)
		{
			Map<HAction, Integer> proposals = new HashMap<>();
			for (Map<HAction, Integer> guides : guidance.values()) {
				for (Entry<HAction, Integer> entry : guides.entrySet()) {
					HAction action = entry.getKey();
					if (compostate.contains(action)) {
						Integer estimate = entry.getValue();
						Integer current = proposals.get(action);
						if (current == null || current > estimate)
							proposals.put(action, estimate);
					}
				}
			}
			return proposals;
		}
		
		
		/** Returns true if and only if all the available actions from a given
		 *  compostate have a valid proposal. Also updates the advice cache. */
		private boolean checkAdvice(
				Compostate compostate,
				Set<HState> tips,
				Map<HState, Map<HAction, Integer>> guidance,
				Map<HAction, Integer> proposals)
		{
			boolean result = true;
			for (HAction action : compostate.getTransitions())
				if (!proposals.containsKey(action)) {
					result = false;
					break;
				}
			if (result)
				for (HState state : tips)
					advice.put(state, guidance.get(state));
			return result;
		}
		
		
		/** Returns true if the compostate recommendations fulfill an
		 *  acceptable age restriction. This is used to detect when the
		 *  optimization may have lost precision thus forcing the recomputation
		 *  of the abstraction. */
		private boolean checkAge(Compostate compostate, Map<HAction, Integer> ages) {
			int age = 0;
			for (Recommendation recommendation : compostate.recommendations)
				age += ages.get(recommendation.getAction()) == 0 ? 0 : 1;  // counts the number of actions past generation 0
			return age <= 1;
		}
		
		
		/** Extracts from the suggestions map actual recommendations for the composed state.
		 *  Returns true if the recommendations for all the outgoing actions were gathered. */
		private boolean extractRecommendations(Compostate compostate, HTransitionMap suggestions) {
			Map<HState, Map<HAction, Integer>> guidance = new HashMap<>();
			Map<HAction, Integer> ages = new HashMap<>();
			Set<HState> tips = extractAdvice(guidance, ages);
			extractGuidance(suggestions.new Mapit(tips), guidance, ages);
			Map<HAction, Integer> proposals = extractProposals(compostate, guidance);
			boolean result = suggestions == gen0Suggestions ||
				checkAdvice(compostate, tips, guidance, proposals);
			if (result) {
				for (Entry<HAction, Integer> proposal : proposals.entrySet())
					if (compostate.addRecommendation(proposal.getKey(), proposal.getValue()))
						break;
				if (suggestions == gen0Suggestions || checkAge(compostate, ages)) {
					compostate.sortRecommendations();
				} else {
					compostate.recommendations = null;
					result = false;
				}
				//System.out.println((suggestions == gen0Suggestions ? "Monod: " : "Vapid(" + age + "): ") + compostate.recommendations);
			}
			return result;
		}
		
		
		/** Updates an heuristic state suggestion with a new transition if better. */
		private void updateSugestions(Compostate compostate, HState parent, HAction action, HState child, int estimate) {
			allSuggestions.put(parent, action, child, estimate);
			if (parent.generation == 0 && child.generation <= 1 && compostate.contains(action))
				gen0Suggestions.put(parent, action, child, estimate);
		}
		
		
		/** Marks a given child state as a goal in the abstraction. */
		private void addGoal(HState child, HAction action, HState parent, int generation) {
			BinaryRelation<HAction,HState> relation = goals.get(child);
			if (relation == null)
				goals.put(child, relation = new BinaryRelationImpl<>());
			relation.addPair(action, parent);
			parent.estimate = computeEstimate(parent, 0, generation); // can I be setting the estimate twice?
		}
		
		
		/** Computes the estimate for the parent state, considering the child estimate and generation. */
		private int computeEstimate(HState parent, int child, int generation) {
			int estimate = INF;
			if (child != INF) {
				estimate = child + (generation <= parent.generation ? 0 :
						   generation - parent.generation);
				if (estimate > generations)
					estimate = generations + 1;
			}
			return estimate;
		}
		
		
		/** Clears the internal state of the heuristic evaluator. */
		private void clear() {
			for (Entry<Integer, HashMap<State, Integer>> lts : cache.entrySet())
				for (Entry<State, Integer> state : lts.getValue().entrySet())
					stash.get(state.getValue()).clear();
			index = 0;
			update.clear();
			deadlocks.clear();
			errors.clear();
			goals.clear();
			enablers.clear();
			states.clear();
			gen0Suggestions.clear();
			clearSuggestions();
		}
		
		
		/** This class represents an Heuristic State, that is a state in the
		 *  abstraction that is used to estimate the distance to the goal. */
		private class HState {
			
			/** The id of the LTS to which this state belongs. */
			private final int lts;
			
			/** The original state this represents in the abstraction. */
			private final State state;
			
			/** Precomputed hash code. */
			private final int hash;
			
			/** Iteration number when this state stopped being considered an error. */
			private int generation;
			
			/** Estimated distance to the goal. */
			private int estimate;
			
			/** Most promising direction to reach the goal. */
			private HAction direction;
			
			/** Parents of this state in the abstraction. */
			private BinaryRelation<HAction, HState> parents;
			
			/** Parents this state has in the abstraction (pending back-propagation). */
			private Set<HState> unpropagated;
			
			
			/** Constructor for an Heuristic State. */
			public HState(int lts, State state) {
				this.lts = lts;
				this.state = state;
				this.parents = new BinaryRelationImpl<>();
				this.unpropagated = new HashSet<>();
				this.hash = stash.size();
				stash.add(this);
				clear();
			}
			
			
			/** Returns the outgoing transitions from this state in its own LTS. */
			public BinaryRelation<Action,State> getTransitions() {
				return ltss.get(lts).getTransitions(state); // accessing LTSs
			}
			
			
			/** Updates the most promising direction of this state given a new
			 *  action and estimate. It takes into consideration the type of
			 *  the state, choosing the minimum action if controllable. For
			 *  mixed and uncontrollable states we also choose the minimum
			 *  in order to preserve admissibility despite that it may be
			 *  too optimistic. */
			public int updateDirection(HAction action, HState child, int estimate) {
				int changes = 0;
				boolean equalActions = action.equals(direction);
				if (this.estimate == UNDEF || this.estimate > estimate) { // optimistic
					this.estimate = estimate;
					direction = action;
					changes = equalActions ? 1 : 2;
					unpropagated.remove(child);
				}
				return changes;
			}
			
			
			/** Returns the parents of this state in the abstraction. */
			public BinaryRelation<HAction,HState> getParents() {
				return parents;
			}
			
			
			/** Adds a new parent to this state. */
			public void addParent(HAction action, HState state) {
				parents.addPair(action, state);
				unpropagated.add(state);
			}
			
			
			/** Sets the generation to this state if novel. */
			public HState setGeneration(int generation) {
				if (this.generation == UNDEF)
					this.generation = generation;
				return this;
			}
			
			
			/** Clears the information on this state to allow reusing it. */
			public void clear() {
				generation = UNDEF;
				estimate = UNDEF;
				direction = null;
				unpropagated.clear();
				parents.clear();
			}
			
			
			/** The hash code for this state. */
			@Override
			public int hashCode() { return hash; }
			
			
			/** Returns whether this state is equals to a given object. */
			@Override
			public boolean equals(Object obj) {
				boolean result = false;
				if (obj != null && obj instanceof DirectedControllerSynthesis.HeuristicEvaluator.HState) {
					@SuppressWarnings("unchecked")
					HState other = (HState)obj;
					result = this.lts == other.lts &&
							 this.state.equals(other.state);
				}
				return result;
			}
			
			
			/** Returns the string representation of this state. */
			@Override
			public String toString() {
				String machine = names.get(lts); 
				int dot = machine.lastIndexOf('.');
				if (dot != -1)
					machine = machine.substring(dot+1);
				return "[" + state.toString() + "]_" + machine;
			}
			
		}
		
		
		/** Low-memory/high-performance Set of HStates implementation. */
		private class HSet extends AbstractSet<HState> {
			
			/** Internal representation as a bitset. */
			private BitSet bitset;
			
			/** Set cardinality. */
			private int size;
			
			/** Constructor for a HSet. */
			public HSet() {
				bitset = new BitSet();
				size = 0;
			}
			
			/** Returns true if this set contains the state. */
			@Override
			public boolean contains(Object state) {
				return bitset.get(state.hashCode());
			}
			
			/** Adds a given state to this set and returns true if it was not contained. */
			@Override
			public boolean add(HState state) {
				boolean result = !contains(state);
				bitset.set(state.hashCode());
				if (result) {
					size++;
				}
				return result;
			}
			
			/** Removes a give state from this set and returns true if it was contained. */
			@Override
			public boolean remove(Object state) {
				boolean result = contains(state);
				bitset.clear(state.hashCode());
				if (result) {
					size--;
				}
				return result;
			}
			
			/** Clears this set. */
			@Override
			public void clear() {
				bitset.clear();
				size = 0;
			}
			
			/** Returns the number of states in this set. */
			@Override
			public int size() {
				return size;
			}
			
			/** Returns whether two HSets are equal. */
			@Override
			public boolean equals(Object obj) {
				return obj != null && obj.equals(bitset);
			}
			
			/** Returns the hash code for this set. */
			@Override
			public int hashCode() {
				return bitset.hashCode();
			}
			
			/** Returns an iterator for this set. */
			@Override
			public Iterator<HState> iterator() {
				return new Bitit();
			}
			
			/** This class implements an iterator for an HSet. */
			private class Bitit implements Iterator<HState> {
				
				/** Index of the next element to be iterated. */
				private int index = bitset.nextSetBit(0);
				
				/** Index of the previous element iterated. */
				private int prev  = -1;

				/** Returns whether there are more elements to be iterated. */
				@Override
				public boolean hasNext() {
					return index != -1;
				}

				/** Returns the next element to be iterated and advences the iterator. */
				@Override
				public HState next() {
					HState result = stash.get(index);
					prev = index;
					index = bitset.nextSetBit(index+1);
					return result;
				}

				/** Removes from the set the last iterated element. */
				@Override
				public void remove() {
					bitset.clear(prev);
					size--;
				}
				
			}
			
		}
		
		
		/** This class represents a transition in the abstraction, holding the
		 *  parent and child states and the action relating them. */
		private class HTransition {
			
			/** Source state of the transition. */
			private HState parent;
			
			/** Label of the transition. */
			private HAction action;
			
			/** Target state of the transition. */
			private HState child;
			
			/** Estimated distance to a goal for this transition. */
			private int estimate;
			
			/** Copies to this transitions the values of another. */
			public void copy(HTransition other) {
				this.parent = other.parent;
				this.action = other.action;
				this.child = other.child;
				this.estimate = other.estimate;
			}
			
			/** Returns the String representation of this Transition. */
			@Override
			public String toString() { return parent + "---" + action + "(" + estimate + ")-->" + child; }		
			
		}
		
		
		/** Performant mapping of transitions. */
		private class HTransitionMap implements Iterable<HTransition> {
			
			/** Internal representation as maps of maps. */
			private Map<HState, Map<HAction, Map<HState, Integer>>> transitions;
			
			/** Constructor for a HTransitionMap. */
			public HTransitionMap() {
				transitions = new HashMap<>();
			}
			
			/** Clears this map, removing all transitions. */
			public void clear() {
				transitions.clear();
			}
			
			/** Returns whether this map is empty. */
			public boolean isEmpty() {
				return transitions.isEmpty();
			}
			
			/** Assigns the mapping of transition (parent,action,child) with a
			 *  given estimate. If the transition already has an estimate the
			 *  map keeps the lesser. */
			public void put(HState parent, HAction action, HState child, Integer estimate) {
				Map<HAction, Map<HState, Integer>> actions = transitions.get(parent);
				if (actions == null)
					transitions.put(parent, actions = new HashMap<>());
				Map<HState, Integer> children = actions.get(action);
				if (children == null)
					actions.put(action, children = new HashMap<>());
				Integer current = children.get(child);
				if (current == null || current > estimate)
					children.put(child, estimate);
			}
			
			/** Returns the string representation of this map */
			@Override
			public String toString() {
				String result = "[";
				Iterator<HTransition> it = iterator();
				while (it.hasNext())
					result += it.next().toString() + ", ";
				if (result.length() > 1)
					result = result.substring(0, result.length()-2);
				result += "]";
				return result;
			}

			/** Returns an iterator to all the transitions in the map. */
			public Iterator<HTransition> iterator() {
				return new Mapit(transitions.keySet());
			}
			
			/** This class implements an iterator for the HTransitionMap. */
			private class Mapit implements Iterator<HTransition> {
				
				/** Iterator to transitions from a given parent state. */
				Iterator<HState> pit;
				
				/** Iterator to transitions from a given state and action. */
				Iterator<Entry<HAction, Map<HState, Integer>>> ait;
				
				/** Iterator to transitions from a given state, action and child. */
				Iterator<Entry<HState, Integer>> cit;
				
				/** Current transitions (next transition to be returned by the next method). */
				HTransition current;
				
				/** Last transition returned by the next method. */
				HTransition last; // used to avoid unnecessary allocations
				
				/** Indicates if the current transition is valid or not. */
				boolean valid;
				
				/** Constructor for Mapit. */
				public Mapit(Set<HState> ps) {
					pit = ps.iterator();
					ait = Collections.emptyIterator();
					cit = Collections.emptyIterator();
					current = new HTransition();
					last = new HTransition();
					next();
				}
				
				/** Returns whether this iterator has more transitions to iterate. */
				@Override
				public boolean hasNext() {
					return valid;
				}
				
				/** Returns (in code) which of the internal iterators need to
				 *  be updated to pass to the next transition. */
				private int nextCase() {
					return  cit.hasNext() ? 3 :
							ait.hasNext() ? 2 :
							pit.hasNext() ? 1 : 0;
				}

				/** Returns the following transition to iterate and advances
				 *  the iterator. The returned transition has aliasing with
				 *  other transitions returned by this iterator. */
				@Override
				public HTransition next() {
					valid = false;
					last.copy(current);
					switch (nextCase()) {
						case 1:
							HState parent = null;
							Map<HAction, Map<HState, Integer>> parentTransitions = null;
							do {
								parent = pit.next();
								parentTransitions = transitions.get(parent);
							} while (parentTransitions == null && pit.hasNext());
							if (parentTransitions == null)
								break;
							ait = parentTransitions.entrySet().iterator();
							current.parent = parent;
						case 2:
							Entry<HAction, Map<HState, Integer>> actions = ait.next();
							cit = actions.getValue().entrySet().iterator();
							current.action = actions.getKey();
						case 3:
							Entry<HState, Integer> children = cit.next();
							current.child = children.getKey();
							current.estimate = children.getValue();
							valid = true;
					}
					return last;
				}

				/** Removing transitions is unsuported by this iterator and
				 * throws UnsupportedOperationException(). It is listed here
				 * just to comply with the Iterator interface. */
				@Override
				public void remove() {
					throw new UnsupportedOperationException();
				}
				
			}
			
			
		}
		

	}
	
	
	
	/** This class represents a recommended course of action computed by the
	 *  heuristic procedure. */
	private class Recommendation implements Comparable<Recommendation> {

		/** The action recommended to explore. */
		private HAction action;
		
		/** The estimated distance to the goal. */
		private int estimate;
		
		
		/** Constructor for a recommendation. */
		public Recommendation(HAction action, int estimate) {
			this.action = action;
			this.estimate = estimate;
		}
		
		
		/** Returns the action this recommendation suggests. */
		public HAction getAction() { return action; }
		
		
		/** Returns the estimate distance to the goal for this recommendation. */
		public int getEstimate() { return estimate; }
		
		
		/** Compares two recommendations by (<=). */
		@Override
		public int compareTo(Recommendation o) { return estimate - o.estimate; }
		
		
		/** Returns the string representation of this recommendation. */
		@Override
		public String toString() {
			return action.toString() + "(" + estimate + ")";
		}
		
	}
	
	
	
	/** This class represents a state in the parallel composition of the LTSs
	 *  in the environment. These states are used to build the fragment of the
	 *  environment required to reach the goal on-the-fly. The controller for
	 *  the given problem can then be extracted directly from the partial
	 *  construction achieved by using these states. */
	private class Compostate implements Comparable<Compostate> {
		
		/** States by each LTS in the environment that conform this state. */
		private List<State> states; // Note: should be a set of lists for non-deterministic LTSs
		
		/** The estimated distance to the goal from this state. */
		private int estimate;
		
		/** The real distance to the goal state from this state. */
		private int distance;
		
		/** A ranking of the outgoing transitions from this state. */
		private List<Recommendation> recommendations;
		
		/** An iterator for the ranking of recommendations. */
		private Iterator<Recommendation> recommendit;
		
		/** Current recommendation (or null). */
		private Recommendation recommendation;
		
		/** Indicates whether the state is actively being used. */
		private boolean live;
		
		/** Indicates whether the state is in the open queue. */
		private boolean inOpen;
		
		/** Indicates whether the state is controlled or not. */
		private boolean controlled;
		
		/** Indicates whether the state has live parents or not. */
		private Boolean liveParent;
		
		/** Children states expanded following a recommendation of this state. */
		private BinaryRelation<HAction, Compostate> children;
		
		/** Parents that expanded into this state. */
		private BinaryRelation<HAction, Compostate> parents;
		
		/** Set of actions enabled from this state. */
		private Set<HAction> transitions;
		
		/** Set of actions that are known to lead to goals. */
		private Set<HAction> objective;
		
		
		/** Constructor for a Composed State. */
		public Compostate(List<State> states) {
			this.states = states;
			this.distance = INF;
			this.estimate = UNDEF;
			this.live = false;
			this.inOpen = false;
			this.controlled = true; // we assume the state is controlled until an uncontrollable recommendation is made
			this.children = new BinaryRelationImpl<>();
			this.parents = new BinaryRelationImpl<>();
			this.objective = new HashSet<>();
			buildTransitions();
		}
		
		
		/** Returns the states that conform this composed state. */
		public List<State> getStates() {
			return states;
		}
		
		
		/** Returns whether a given action is enabled from this compostate. */
		public boolean contains(HAction action) {
			return getTransitions().contains(action);
		}
		
		
		/** Returns the distance from this state to the goal state (INF if not yet computed). */
		public int getDistance() {
			return distance;
		}
		
		
		/** Sets the distance from this state to the goal state. */
		public void setDistance(int distance) {
			this.distance = distance;
		}
		
		
		/** Updates this state distance considering the distance of one of its children. */
		public void updateDistance(HAction action) {
			int dist = getChild(action).getDistance();
			if (dist < INF)
				dist++;
			if (dist < distance)
				distance = dist;
		}
		
		
		/** Returns the estimated distance from this state to a goal state. */
		public int getEstimate() {
			return estimate;
		}
		
		
		/** Sets the estimated distance from this state to a goal state. */
		public void setEstimate(int estimate) {
			this.estimate = estimate;
		}
		
		
		/** Indicates whether this state has been evaluated, that is, if it has
		 *  a valid ranking of recommendations. */
		public boolean isEvaluated() {
			return recommendations != null;
		}
		
		
		/** Sorts this state's recommendations in order to be iterated properly. */
		public void sortRecommendations() {
			Collections.sort(recommendations);
			if (!isControlled())
				Collections.reverse(recommendations);
			initRecommendations();
		}
		
		
		/** Adds a new recommendation to this state and returns whether an
		 *  error action has been introduced (no other recommendations should
		 *  be added after an error is detected).
		 *  Recommendations should not be added after they have been sorted and
		 *  with an iterator in use. */
		public boolean addRecommendation(HAction action, int estimate) {
			if (recommendations == null)
				recommendations = new ArrayList<>();
			boolean uncontrollableAction = !action.isControllable();
			if (controlled) { // may not work with lists of recommendations 
				if (uncontrollableAction) {
					controlled = false;
					recommendations.clear(); // alternatively I could detect if the state is controllable or not and add only the corresponding recommendations
				}
			}
			if ((controlled || uncontrollableAction) && estimate != INF) // update recommendation
				recommendations.add(new Recommendation(action, estimate));
			boolean error = !controlled && uncontrollableAction && estimate == INF;
			if (error) // an uncontrollable state with at least one INF estimate is an automatic error
				recommendations.clear();
			return error;
		}
		
		
		/** Returns whether the iterator points to a valid recommendation. */
		public boolean hasValidRecommendation() {
			return isEvaluated() && recommendation != null;
		}
		
		
		/** Advances the iterator to the next recommendation. */
		public Recommendation nextRecommendation() {
			Recommendation recommendation = this.recommendation;
			updateRecommendation();
			return recommendation;
		}
		
		
		/** Initializes the recommendation iterator guaranteeing representation invariants. */
		private void initRecommendations() {
			recommendit = recommendations.iterator();
			updateRecommendation();
		}
		
		
		/** Initializes the recommendation iterator and current estimate for the state. */
		private void updateRecommendation() {
			if (recommendit.hasNext()) {
				this.recommendation = recommendit.next();
				setEstimate(this.recommendation.getEstimate()); // update this state estimate in case the state is reopened
			} else {
				this.recommendation = null;
			}
		}
		
		
		/** Clears all recommendations from this state. */
		public void clearRecommendations() {
			if (isEvaluated()) {
				recommendations.clear();
				recommendit = null;
				recommendation = null;
			}
		}
		
		
		/** Returns whether this state is being actively used. */
		public boolean isLive() {
			return live;
		}
		
		
		/** Returns whether this state has a live parent. */
		public boolean hasLiveParent() {
			boolean valid = false;
			if (liveParent != null) { // cache the result while the parent set does not change
				valid = liveParent;
			} else {
				Iterator<Pair<HAction, Compostate>> pit = parents.iterator();
				while (!valid && pit.hasNext())
					valid |= pit.next().getSecond().isLive();
			}
			return valid;
		}
		
		
		/** Adds this state to the open queue (reopening it if previously closed). */
		public void open() {
			live = true;
			if (!inOpen) {
				if (getChildren().isEmpty()) {
					inOpen = true;
					open.add(this);
				} else { // we are reopening a state, thus we reestablish it's children instead
					boolean empty = true;
					for (Pair<HAction,Compostate> transition : getChildren()) {
						Compostate child = transition.getSecond();
						if (!child.isLive() && !isGoal(child)) {
							child.open();
							empty = false;
						}
					}
					if (inOpen = (empty || isControlled()))
						open.add(this);
				}
			}
		}
		
		
		/** Closes this state to avoid further exploration. */
		public void close() {
			live = false;
			// open.remove(this);
		}
		
		
		/** Returns whether this state is controllable or not. */
		public boolean isControlled() {
			return controlled;
		}
		
		
		/** Returns the set of actions enabled from this composed state. */
		public Set<HAction> getTransitions() {
			return transitions;
		}
		

		/** Initializes the set of actions enabled from this composed state. */
		private void buildTransitions() { // Note: here I can code the wia and ia behavior for non-deterministic ltss
			transitions = new HashSet<>();
			if (facilitators == null) {
				for (int i = 0; i < states.size(); ++i) {
					for (Pair<Action,State> transition : ltss.get(i).getTransitions(states.get(i))) {
						HAction action = alphabet.getHAction(transition.getFirst());
						allowed.add(i, action);
					}
				}
			} else {
				for (int i = 0; i < states.size(); ++i)
					if (!facilitators.get(i).equals(states.get(i)))
						for (Pair<Action,State> transition : ltss.get(i).getTransitions(facilitators.get(i))) {
							HAction action = alphabet.getHAction(transition.getFirst());
							allowed.remove(i, action); // remove old non-shared facilitators transitions  
						}
				for (int i = 0; i < states.size(); ++i)
					if (!facilitators.get(i).equals(states.get(i)))
						for (Pair<Action,State> transition : ltss.get(i).getTransitions(states.get(i))) {
							HAction action = alphabet.getHAction(transition.getFirst());
							allowed.add(i, action); // add new non-shared facilitators transitions
						}
			}
			transitions.addAll(allowed.getEnabled());
			facilitators = states;
		}
		
		
		/** Adds an expanded child to this state. */
		public void addChild(HAction action, Compostate child) {
			children.addPair(action, child);
		}
		
		
		/** Removes a closed child from this state. */
		public void removeChild(HAction action) {
			for (Compostate child : children.getImage(action)) // remove all non-deterministic children
				children.removePair(action, child);
		}
		
		
		/** Returns all transition leading to children of this state. */
		public BinaryRelation<HAction,Compostate> getChildren() {
			return children;
		}
		
		
		/** Returns one child following a given action (not sensible in non-deterministic environments). */
		public Compostate getChild(HAction action) {
			return children.getImage(action).iterator().next();
		}
		
		
		/** Adds an expanded parent to this state. */
		public void addParent(HAction action, Compostate parent) {
			parents.addPair(action, parent);
		}
		
		
		/** Removes a closed parent from this state. */
		public void removeParent(HAction action, Compostate parent) {
			parents.removePair(action, parent);
			liveParent = null;
		}
		
		
		/** Clears all parents for this state. */
		public void releaseParents() {
			parents = new BinaryRelationImpl<>();
			liveParent = null; // unnecessary
		}
		
		
		/** Returns the inverse transition leading to parents of this state. */
		public BinaryRelation<HAction,Compostate> getParents() {
			return parents;
		}
		
		
		/** Marks an action as a step in the path to a goal state. */
		public void addObjective(HAction action) {
			objective.add(action);
		}
		
		
		/** Returns whether a given action leads to a goal from this state. */
		public boolean isObjective(HAction action) {
			return objective.contains(action);
		}
		
		
		/** Compares two composed states by their estimated distance to a goal by (<=). */
		@Override
		public int compareTo(Compostate o) {
			return estimate - o.estimate;
		}
		
		
		/** Returns the string representation of a composed state. */
		@Override
		public String toString() {
			return states.toString();
		}
		
	}
	
	
	
	/** This class encapsulates the alphabet of the plant.
	 *  Provides the required functionality for actions with perfect hashing.
	 *  @see HAction*/
	public class Alphabet implements Iterable<HAction> {
		
		/** Actions indexed by hash. */
		private List<Action> actions;
		
		/** Map of actions to their respective hash codes. */
		private Map<Action, HAction> codes;
		
		/** Controllable action flags indexed by hash. */
		private BitSet controlbits;
		
		/** Error action flags indexed by hash. */
		private BitSet errorbits;
		
		/** Goal action flags indexed by hash. */
		private BitSet goalbits;
		
		
		/** Constructor for the Alphabet. */
		public Alphabet() {
			actions = new ArrayList<>(ltss.size());
			codes = new HashMap<>();
			controlbits = new BitSet();
			errorbits = new BitSet();
			goalbits = new BitSet();
			alphabet = this;
			init();
		}
		
		
		/** Initialization. Populates the alphabet with the transitions from the LTSs. */
		private void init() {
			for (int i = 0; i < ltss.size(); ++i) {
				Set<HAction> actionsOf_i = new HashSet<HAction>();
				for (Action action : ltss.get(i).getActions()) {
					HAction code = codes.get(action);
					if (code == null)
						codes.put(action, code = new HAction(action));
					actionsOf_i.add(code);
				}
			}
		}
		
		/** Returns the size of the alphabet (the number of actions). */
		public int size() {
			return actions.size();
		}
		
		/** Provides an iterator for the actions in the alphabet. */
		@Override
		public Iterator<HAction> iterator() {
			return codes.values().iterator();
		}
		
		/** Given an action returns its perfectly hashed proxy. */
		public HAction getHAction(Action action) {
			return codes.get(action);
		}
		
	}
	
	
	
	/** This class serves as a proxy for the Action type argument.
	 *  Internally it is represented simply by its hash value.
	 *  Together with the alphabet allows for a high-performance
	 *  representation of transitions. */
	public class HAction {
		
		/** Hash value for this action. */
		private final int hash;
		
		
		/** Constructor for an HAction given a common Action. */
		public HAction(Action action) {
			hash = alphabet.actions.size();
			alphabet.actions.add(action);
			if (controllable.contains(action))
				alphabet.controlbits.set(hash);
			if (reach.contains(action))
				alphabet.goalbits.set(hash);
			else if (avoid.contains(action))
				alphabet.errorbits.set(hash);
		}
		
		
		/** Returns true if two HActions are equals. */
		@Override
		public boolean equals(Object obj) {
			boolean result = false;
			if (obj != null && obj instanceof DirectedControllerSynthesis.HAction) {
				@SuppressWarnings("unchecked")
				HAction other = (HAction)obj;
				result = this.hash == other.hash; 
			}
			return result;
		}
		
		
		/** Returns the hash code for this HAction. */
		@Override
		public int hashCode() {
			return hash;
		}
		
		
		/** Indicates whether this action is a goal. */
		public boolean isGoal() {
			return alphabet.goalbits.get(hash);
		}
		
		
		/** Indicates whether this action is an error. */ 
		public boolean isError() {
			return alphabet.errorbits.get(hash);
		}
		
		
		/** Indicates whether this action is controllable. */
		public boolean isControllable() {
			return alphabet.controlbits.get(hash);
		}
		
		
		/** Returns the Action this HAction proxies. */
		public Action getAction() {
			return alphabet.actions.get(hash);
		}
		
		/** Return the String representation of this HAction. */
		@Override
		public String toString() {
			return getAction().toString();
		}
		
	}
	
	
	/** This class represents a set of synchronized transitions, that is to say,
	 *  that a transition belongs to the set if all intervening LTSs allow the
	 *  action. */
	public class TransitionSet implements Cloneable {
		
		/** Maps actions to the number of LTSs enabling them. */
		private int[] enabledCount;
		
		/** The set of enabled transitions by each LTS. */
		private List<BitSet> enabledByLTS;
		
		/** The set of enabled transitions. */
		private Set<HAction> enabledActions;
		
		
		/** Constructor for a Transition Set (avoids initialization if alphabet is null). */
		public TransitionSet(Alphabet alphabet) {
			if (alphabet != null)
				init(alphabet);
		}
		
		
		/** Initialization for a Transition Set, marks as enabled for each LTS
		 *  all the actions that do not belong to its alphabet. */
		private void init(Alphabet alphabet) {
			enabledByLTS = new ArrayList<>(ltss.size());
			enabledCount = new int[alphabet.size()];
			enabledActions = new HashSet<>();
			for (int i = 0; i < ltss.size(); ++i)
				enabledByLTS.add(new BitSet(alphabet.size()));
			for (HAction hAction : alphabet) {
				Action action = hAction.getAction();
				int hash = hAction.hashCode();
				int count = 0;
				for (int i = 0; i < ltss.size(); ++i) {
					Set<Action> actionsOf_i = ltss.get(i).getActions();
					if (!actionsOf_i.contains(action)) {
						enabledByLTS.get(i).set(hash);
						count++;
					}
				}
				enabledCount[hash] = count;
				if (count == ltss.size())
					enabledActions.add(hAction);
			}
		}
		
		
		/** Adds an action enabled by the given LTS.
		 *  Returns true if the action was not enabled and it became enabled
		 *  as a consequence of this addition. */
		public boolean add(int lts, HAction action) {
			boolean result = false;
			int hash = action.hashCode();
			if (!enabledByLTS.get(lts).get(hash)) {
				enabledByLTS.get(lts).set(hash);
				int count = ++enabledCount[hash];
				result = count == ltss.size();
			}
			if (result)
				enabledActions.add(action);
			return result;
		}
		
		
		/** Removes an action enabled by the given LTS.
		 *  Returns true if the action was enabled and it became disabled
		 *  as a consequence of this removal. */
		public boolean remove(int lts, HAction action) {
			boolean result = false;
			int hash = action.hashCode();
			if (enabledByLTS.get(lts).get(hash)) {
				enabledByLTS.get(lts).clear(hash);
				int count = enabledCount[hash]--;
				result = count == ltss.size();
			}
			if (result)
				enabledActions.remove(action);
			return result;
		}
		
		
		/** Returns the set of enabled actions of this Transition Set. */
		public Set<HAction> getEnabled() {
			return enabledActions;
		}
		
		/** Returns whether an action belongs to this Transition Set.
		 *  That is, if all intervening LTSs allow the action. */
		public boolean contains(HAction action) {
			return enabledCount[action.hashCode()] == ltss.size();
		}
		
		
		/** Returns a new instance of a Transition Set by deep copying this. */
		@Override
		public TransitionSet clone() {
			TransitionSet result = new TransitionSet(null);
			result.enabledByLTS = new ArrayList<>(this.enabledByLTS.size());
			result.enabledActions = new HashSet<>(this.enabledActions);
			result.enabledCount = new int[this.enabledCount.length];
			System.arraycopy(this.enabledCount, 0, result.enabledCount, 0, this.enabledCount.length);
			for (int i = 0; i < this.enabledByLTS.size(); ++i) {
				BitSet enabledBy_i = new BitSet();
				enabledBy_i.or(enabledByLTS.get(i));
				result.enabledByLTS.add(enabledBy_i);
			}
			return result;
		}
		
		
		/** Overrides the internal representation of this Transition Set to
		 *  reflect that of another set. */
		public void copy(TransitionSet other) {
			enabledActions.clear();
			enabledActions.addAll(other.enabledActions);
			System.arraycopy(other.enabledCount, 0, this.enabledCount, 0, this.enabledCount.length);
			for (int i = 0; i < this.enabledByLTS.size(); ++i) {
				BitSet enabledBy_i = this.enabledByLTS.get(i);
				enabledBy_i.clear();
				enabledBy_i.or(other.enabledByLTS.get(i));
			}
		}
		
		
		/** Returns a string representation of this set. */
		@Override
		public String toString() {
			return enabledActions.toString();
		}
		
	}
	
	
	
	/** This class holds statistic information about the heuristic procedure. */
	public class Statistics {
		
		/** Number of expanded states. */
		private int expanded;
		
		/** Number of actually evaluated states. */
		private int evaluated;
		
		/** Number of revisited nodes, that is the number of additional
		 *  recommendations beyond the first were used. */
		private int revisited;
		
		/** Number of states in the generated controller. */
		private int used;
		
		/** System wall clock in milliseconds at the start of the procedure. */
		private long started;
		
		/** System wall clock in milliseconds at the end of the procedure. */
		private long ended;
		
		///** Memory consumption before HCS started. */
		//private long base;
		
		/** Best estimated distance to the goal achieved so far. */
		private int estimate = INF;
		
		
		/** Increments the number of expended states. */
		public synchronized void incExpanded() {
			expanded++;
		}

		/** Increments the number of evaluated states. */
		public synchronized void incEvaluated() {
			evaluated++;
		}
		
		
		/** Increments the number of revisited states, that is states for which
		 *  a recommendation beyond the first were used. */
		public synchronized void incRevisited() {
			revisited++;
		}
		
		
		/** Sets the number of states used in the generated controller. */
		public void setUsed(int used) {
			this.used = used;
		}
		
		/** Marks the start of the procedure to measure elapsed time. */
		public void start() {
			started = System.currentTimeMillis();
			//base = getUsedMemory();
		}

		/** Marks the end of the procedure to measure elapsed time. */
		public void end() {
			ended = System.currentTimeMillis();
		}
		
		/** Indicates if the heuristic procedure is running. */
		public boolean isRunning() {
			return ended == 0;
		}
		
		/** Sets the current estimated distance to the goal (if better). */
		public void setEstimate(int estimate) {
			if (this.estimate > estimate)
				this.estimate = estimate;
		}
		
		/** Returns the number of expanded states (so far). */
		public int getExpanded() {
			return expanded;
		}
		
		/** Returns the current estimate distance to the goal. */
		public int getEstimate() {
			return estimate;
		}
		
		/** Returns the elapsed time from the start of the execution up to now. */
		public long getElapsed() {
			return System.currentTimeMillis() - started;
		}
		
		/** Returns an approximation of the memory used by the procedure. */
		public long getUsedMemory() {
			Runtime runtime = Runtime.getRuntime();
			return runtime.totalMemory() - runtime.freeMemory() /*- base*/;
		}
		
		/** Resets the statistics. */
		public void clear() {} // to do, but not really necessary 
		
		/** Returns a string with the statistic data. */
		@Override
		public String toString() {
			return  "Expanded: " + expanded + "\n" +
					"Evaluated: " + evaluated + "\n" +
					"Revisited: " + revisited + "\n" +
					"Used: " + used + "\n" +
					"Elapsed: " + (ended - started) + " ms\n";
		}
		
		/** Returns a string with live statistics. */
		public String toLive() {
			return  "  " + expanded + " states expanded " + // - estimated distance " + estimate // not very informative
					" (" + formatTime(getElapsed()) + ", " + formatMemory(getUsedMemory()) + ")";
		}
		
		/** Returns tab separated values with statistic data (expanded, evaluated, revisited, used, elapsed).*/
		public String toTSV() {
			return expanded + "\t" + evaluated + "\t" + revisited + "\t" + used + "\t" + (ended - started) + "\n";
		}

		/** Returns time in human readable format. */
		private String formatTime(long time) {
			long sec = 1000;
			long min = 60 * sec;
			long hour = 60 * min;
			String result = "";
			String space = "";
			if (time > hour) {
				result += (time / hour) + " h";
				time %= hour;
				space = " ";
			}
			if (time > min) {
				result += space + (time / min) + " min";
				time %= min;
				space = " ";
			}
			if (time > sec) {
				result += space + (time / sec) + " s";
				time %= sec;
			}
			return result;
		}
		
		/** Returns memory in human readable format. */
		private String formatMemory(long memory) {
			double kilo = 1024;
			double mega = 1024 * kilo;
			double giga = 1024 * mega;
			String result;
			if (memory > giga)
				result = String.format("%02.2f", memory / giga) + " GB";
			else if (memory > mega)
				result = String.format("%02.2f",memory / mega) + " MB";
			else if (memory > kilo)
				result = String.format("%02.2f",memory / kilo) + " KB";
			else
				result = memory + " Bs";
			return result;
		}
		
	}
	
	
}
